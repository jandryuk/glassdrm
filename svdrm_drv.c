//
// SVdrm: SecureView's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/component.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include "svdrm_drv.h"
#include "svdrm_pv.h"

int dirty_debug = 0;
module_param(dirty_debug, int, S_IRUGO | S_IWUSR);

// Store the state of the svdrm device
struct svdrm_device *svdrm_dev;
struct drm_device *drm_dev;

#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,5)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 11, 0))
void
svdrm_unload(struct drm_device *dev)
{
    struct svdrm_device *svdevice = NULL;

    __SVDRM_TRACE__;

    if (!dev) {
      return;
    }

    svdevice = dev->dev_private;

    if (!svdevice) {
      return;
    }

    if (svdevice->cached_cursor) {
      vfree(svdevice->cached_cursor);
      svdevice->cached_cursor = NULL;
    }

    flush_workqueue(svdevice->reconnect_queue);
    destroy_workqueue(svdevice->reconnect_queue);

    // Tear down the provider
    svdrm_teardown_provider();

    // Free up the main svdrm device
    kvfree(svdevice);
    dev->dev_private = NULL;

    return;
}
#else
int
svdrm_unload(struct drm_device *dev)
{
    struct svdrm_device *svdevice = NULL;

    __SVDRM_TRACE__;

    if (!dev) {
      return -ENODEV;
    }

    svdevice = dev->dev_private;

    if (!svdevice) {
      return -ENODEV;
    }

    if (svdevice->cached_cursor) {
      vfree(svdevice->cached_cursor);
      svdevice->cached_cursor = NULL;
    }

    flush_workqueue(svdevice->reconnect_queue);
    destroy_workqueue(svdevice->reconnect_queue);

    // Tear down the provider
    svdrm_teardown_provider();

    // Free up the main svdrm device
    kvfree(svdevice);
    dev->dev_private = NULL;

    return 0;
}
#endif

#ifdef DEBUG
    extern unsigned int drm_debug;
#endif

static int32_t
svdrm_load(struct drm_device *dev, unsigned long flags)
{
    int32_t rc = 0, i = 0;

#ifdef DEBUG
    drm_debug = DRM_UT_KMS;
#endif

    __SVDRM_TRACE__;

    hash_init(svdrm_dev->kms_map);

    // Allocate enough memory so that we can save a copy of the cursor
    svdrm_dev->cached_cursor = vmalloc(CURSOR_SIZE);
    if (!svdrm_dev->cached_cursor) {
        svdrm_error("Could not allocate memory for cursor\n");
        return -ENOMEM;
    }

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,8,0) || LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
    // Give the device a n unique name that it will be seen as by other drivers
    // If for some reason we are unable to set a name, error out
    rc = drm_dev_set_unique(svdrm_dev->dev, DRIVER_NAME);
    if (rc) {
        goto err;
    }
#endif

    // Initialize our mutex lock
    mutex_init(&svdrm_dev->lock);

    // Set up the default connector_groups
    for (i = 0; i < MAX_DISPLAYS; i++) {
        struct svdrm_kms_mapping *mapping = NULL;

        // Allocate space for our mapping information, setup a
        // backpointer to the svdevice and add the mapping to
        // the list of available_mappings
        mapping = kzalloc(sizeof(*mapping), GFP_KERNEL);
        if (!mapping) {
            rc = -ENOMEM;
            goto err;
        }

        // Set default initial values for each mapping. Initialize key to -1,
        // as this is invalid key for a renderable guest (non gpu-pt). At start,
        // Each mapping is not connected. Add each mapping to hash table.
        mapping->dev = svdrm_dev;
        mapping->key = (uint32_t)(-1 - i);
        mapping->display_hint.x = 0;
        mapping->display_hint.y = 0;
        mapping->display_hint.width = 1920;
        mapping->display_hint.height = 1080;
        mapping->connected = false;
        mapping->connector_registered = false;
        mapping->pending_removal = false;
        mapping->buffer_needs_resize = false;
        mapping->buffer_type = __SVDRM_BUFFER_INVALID;
        __svdrm_kms_hash_add(svdrm_dev, mapping, mapping->key);
    }

    // Used to determing if we need to purge any displays
    svdrm_dev->displays_needs_update = false;

    // Initializes the number of advertised displays to 0 since we didn't recieve any yet
    // As well as the number of configured displays
    svdrm_dev->advertised_display_count = 0;
    svdrm_dev->configured_displays = 0;

    // Initialize the mode setting core
    rc = svdrm_kms_init(svdrm_dev);
    if (rc) {
        goto err;
    }

    // Set up a workqueue to handle reconnects
    svdrm_dev->reconnect_queue = create_singlethread_workqueue("svdrm_reconnect_queue");
    if (!svdrm_dev->reconnect_queue) {
        goto err;
    }

    // Initialize PV Display provider
    rc = svdrm_reconnect_provider(NULL);
    if (rc) {
        goto err;
    }

    svdrm_dev->driver_status = SVDRM_STATUS_INITIALIZED;
    printk(KERN_INFO "%s version %d.%d.%d is successfully loaded.", DRIVER_NAME, DRIVER_MAJOR, DRIVER_MINOR, DRIVER_MICRO);
    return 0;

err:
    svdrm_unload(dev);
    return rc;
}

void
svdrm_print_query_info(query_display_info_t *info) {
    uint32_t i = 0;

    if (!info) {
        return;
    }

    for (i = 0; i < MAX_DISPLAYS; i++) {
        svdrm_debug("Connector: %s Key: %d, WidthxHeight: %dx%d, X,Y: %d,%d\n",
        info->connector_names[i], info->display_hint[i].key, info->display_hint[i].width,
        info->display_hint[i].height, info->display_hint[i].x, info->display_hint[i].y);
    }

    return;
}

static int32_t
__get_all_display_information(query_display_info_t *info) {
    uint32_t i, j = 0;
    struct svdrm_kms_mapping *mapping = NULL;

    if (!info) {
        return -ENOENT;
    }

    /* For each mapping, copy over the connector name and its hint information */
    hash_for_each(svdrm_dev->kms_map, i, mapping, hash_node) {
        if (!mapping) {
            continue;
        }

        info->display_hint[j] = mapping->display_hint;
        strncpy(info->connector_names[j], mapping->connector_group.connector.name, MAX_NAME_SIZE);
        j++;
    }

    #if (RHEL_RELEASE_CODE != 0)
        info->is_rhel = true;
    #else
        info->is_rhel = false;
    #endif

    svdrm_print_query_info(info);
    return 0;
}

static int32_t
__set_all_display_information(query_display_info_t *info)
{
    uint32_t hash_index, i;
    struct svdrm_kms_mapping *mapping = NULL;

    if (!info) {
        return -ENOENT;
    }
    hash_index = 0;
    for (i = 0; i < info->num_displays; i++) {
        hash_for_each(svdrm_dev->kms_map, hash_index, mapping, hash_node) {
            if (mapping->key != info->display_hint[i].key) {
                continue;
            }
            mapping->display_hint.x = info->display_hint[i].x;
            mapping->display_hint.y = info->display_hint[i].y;
            mapping->display_hint.width = info->display_hint[i].width;
            mapping->display_hint.height = info->display_hint[i].height;
        }
    }

    return 0;
}

static long
svdrm_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    int32_t ret = 0;
    query_display_info_t displays_info;

    switch (cmd) {
        case SVDRM_IOCTL_GET_STATUS:
            /* Let mosaic_app know the current running state of svdrm*/
            if (copy_to_user((int32_t *)arg, &svdrm_dev->driver_status, sizeof(int32_t))) {
                svdrm_error("Error copying information to mosaic_app\n");
                return 1;
            }
            break;

        case SVDRM_IOCTL_GET_DISPLAY_INFO:
            /* Get information for all of the displays and send it to mosaic_app */
            displays_info.num_displays = MAX_DISPLAYS;
            __get_all_display_information(&displays_info);
            if (copy_to_user((query_display_info_t *)arg, &displays_info, sizeof(query_display_info_t))) {
                svdrm_error("Error copying information to mosaic_app\n");
                return 1;
            }
            break;

        case SVDRM_IOCTL_UPDATE_GUEST_LAYOUT:
            if (copy_from_user(&displays_info,(query_display_info_t *)arg, sizeof(query_display_info_t))) {
                svdrm_error("Error copying information from mosaic_app\n");
                return 1;
            }
            __set_all_display_information(&displays_info);
            {
                uint32_t i;
                struct dh_display_info displays[displays_info.num_displays];
                for (i = 0; i < displays_info.num_displays; i++) {
                    displays[i].key = displays_info.display_hint[i].key;
                    displays[i].x = displays_info.display_hint[i].x;
                    displays[i].y = displays_info.display_hint[i].y;
                    displays[i].width = displays_info.display_hint[i].width;
                    displays[i].height = displays_info.display_hint[i].height;
                }
                svdrm_dev->provider->advertise_displays(svdrm_dev->provider, displays, displays_info.num_displays);
            }
            break;

        case SVDRM_IOCTL_GET_HOST_LAYOUT_CHANGED:
            if (copy_to_user((bool *)arg, &svdrm_dev->host_layout_changed, sizeof(bool))) {
                svdrm_error("Error copying information to mosaic_app\n");
                return 1;
            }
            svdrm_dev->host_layout_changed = false;
            break;

        default:
           return drm_ioctl(filp, cmd, arg);
           break;
    }

   return ret;
}

static const struct file_operations
svdrm_fops =
{
    .owner = THIS_MODULE,
    .open = drm_open,
    .poll = drm_poll,
    .read = drm_read,
    .llseek = no_llseek,
    .release = drm_release,
    .unlocked_ioctl = svdrm_ioctl,
    .mmap = svdrm_gem_mmap,
#ifdef CONFIG_COMPAT
    .compat_ioctl = drm_compat_ioctl,
#endif
};

static const struct vm_operations_struct
svdrm_gem_vm_ops = {
    .open  = drm_gem_vm_open,
    .close = drm_gem_vm_close,
};

/*
* Main drm driver structure that describes all of its details,
* features that it supports, as well as pointers to the DRM CORE functions
*/
static struct drm_driver svdrm = {
    .driver_features = DRIVER_GEM | DRIVER_MODESET,
    .load = svdrm_load,
    .unload = svdrm_unload,
    .major = DRIVER_MAJOR,
    .minor = DRIVER_MINOR,
    .name = DRIVER_NAME,
    .desc = DRIVER_DESC,
    .date = DRIVER_DATE,
    .fops = &svdrm_fops,
    .dumb_create = svdrm_dumb_create,
    .dumb_destroy = svdrm_dumb_destroy,
    .dumb_map_offset = svdrm_dumb_map_offset,
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE == RHEL_RELEASE_VERSION(7,4)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 7, 0))
    .gem_free_object_unlocked = svdrm_gem_free_object,
#else
    .gem_free_object = svdrm_gem_free_object,
#endif
    .gem_vm_ops = &svdrm_gem_vm_ops,
};

static int
svdrm_pm_prepare(struct device *dev)
{
    __SVDRM_TRACE__
    return 0;
}

static void
svdrm_pm_complete(struct device *dev)
{
    __SVDRM_TRACE__;
}

static int
svdrm_pm_suspend(struct device *dev)
{
    __SVDRM_TRACE__;
    return 0;
}

static int
svdrm_pm_resume(struct device *dev)
{
    __SVDRM_TRACE__;
    return 0;
}

static const struct dev_pm_ops svdrm_pm_ops = {
    .prepare = svdrm_pm_prepare,
    .complete = svdrm_pm_complete,
    .suspend = svdrm_pm_suspend,
    .resume = svdrm_pm_resume,
};


static int
svdrm_platform_probe(struct platform_device *pdev)
{
    struct device *dev = &pdev->dev;
    struct svdrm_device *svdevice = NULL;
    struct drm_device *drm_device = NULL;
    int32_t ret = 0;

    __SVDRM_TRACE__;

    // Allocate and initialize a new DRM device
    drm_device = drm_dev_alloc(&svdrm, dev);
    if (IS_ERR(drm_device)) {
        return PTR_ERR(drm_device);
    }

    // Allocate memory for the overall main context device
    // If for some reason we are unable to allocate memory, error out
    svdevice = kzalloc(sizeof(*svdevice), GFP_KERNEL);
    if (!svdevice) {
        svdrm_error("Could not allocate memroy for the context device\n");
        return -ENOMEM;
    }

    // We want the device to keep a back pointer to our drm_device and our drm_device to have
    // a back pointer to our svdevice, and the svdevice to have access to drm_device
    dev_set_drvdata(dev, drm_device);
    drm_device->dev_private = (void *)svdevice;
    svdrm_dev = svdevice;
    svdrm_dev->dev = drm_device;

    // Register drm device with the system and advertise the device to user space and start normal device operation
    ret = drm_dev_register(drm_device, 0);
    if (ret) {
        goto err_register_cleanup;
    }

    return 0;

err_register_cleanup:
    component_unbind_all(dev, drm_device);
    kfree(svdevice);
    svdevice = svdrm_dev = NULL;
    drm_dev->dev_private = NULL;
    dev_set_drvdata(dev, NULL);
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7, 6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 20, 0))
    drm_dev_unref(drm_device);
#else
    drm_dev_put(drm_device);
#endif

    return ret;
}

static int
svdrm_platform_remove(struct platform_device *pdev)
{
    __SVDRM_TRACE__;

    // Tear down the provider
    svdrm_teardown_provider();

    if(drm_dev) {
      // Unregister the device
      // drm_dev_unregister(drm_dev);
      //
      // // Free the drm_device structure
      // drm_dev_unref(drm_dev);
      //
      // drm_dev = NULL;
    }

    return 0;
}

static struct platform_driver svdrm_platform_driver = {
	.probe		= svdrm_platform_probe,
	.remove		= svdrm_platform_remove,
	.driver		= {
		.owner	= THIS_MODULE,
		.name	= "svdrm_platform",
	},
};

static struct platform_device *svdrm_platform_device = NULL;

static int32_t
__init svdrm_init(void)
{
    int rc = 0;
    struct platform_device *pdev;
    __SVDRM_TRACE__;

    rc = platform_driver_register(&svdrm_platform_driver);
    if (rc < 0) {
      goto fail_driver;
    }

    pdev = platform_device_register_simple(svdrm_platform_driver.driver.name, -1, NULL, 0);
		if (IS_ERR(pdev))
			goto fail_device;

    svdrm_platform_device = pdev;

    return rc;

fail_device:
		platform_driver_unregister(&svdrm_platform_driver);
fail_driver:
    return rc;
}

static void
__exit svdrm_exit(void)
{
    __SVDRM_TRACE__;

    if(svdrm_platform_device) {
        platform_device_unregister(svdrm_platform_device);
        svdrm_platform_device = NULL;
    }

    platform_driver_unregister(&svdrm_platform_driver);
}

/* Register initialization and exit functions */
module_init(svdrm_init);
module_exit(svdrm_exit);

/* Define some module information */
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE(DRIVER_LICENSE);
