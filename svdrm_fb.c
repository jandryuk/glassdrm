///
// SVdrm: SecureView's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#include "svdrm_drv.h"

extern int dirty_debug;

struct pv_display*
__svdrm_get_display_from_fb(struct svdrm_framebuffer *svdrm_fb)
{
    __SVDRM_TRACE__;

    // Make sure we have a good fb to start with
    if (!svdrm_fb) {
        return NULL;
    }

    return svdrm_fb->display;
}

static void
svdrm_fb_destroy(struct drm_framebuffer *fb)
{
    struct svdrm_framebuffer *svdrm_fb = NULL;
    struct svdrm_device *device = NULL;

    __SVDRM_TRACE__;

    if (!fb) {
        return;
    }

    svdrm_debug("Destroying framebuffer[%d]: WidthxHeight of the  %dx%d\n",
                fb->base.id, fb->width, fb->height);

	// Convert the drm_framebuffer to a svdrm_framebuffer and exit if we
	// can not get an svdrm_fb object
	svdrm_fb = to_svdrm_framebuffer(fb);

    if (!svdrm_fb || !svdrm_fb->device) {
        return;
    }

    device = svdrm_fb->device;

    mutex_lock(&device->lock);
    // Cleanup the drm specific framebuffer bits
    if (svdrm_fb->obj) {
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7, 6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 20, 0))
        drm_gem_object_unreference_unlocked(svdrm_fb->obj);
#else
        drm_gem_object_put_unlocked(svdrm_fb->obj);
#endif
        svdrm_fb->obj = NULL;
        svdrm_debug("Unreferenced the gem object\n");
    }
    mutex_unlock(&device->lock);
    drm_framebuffer_cleanup(fb);

    // Finally remove the overall sv fb
    kfree(svdrm_fb);
    svdrm_fb = NULL;

    return;
}

bool line_intersects(uint32_t a1, uint32_t a2, uint32_t b1, uint32_t b2)
{
    return (((a1 >= b1) && (a1 <= b2)) ||
            ((a2 >= b1) && (a2 <= b2)) ||
            ((a1 <= b1) && (a2 >= b2)) ||
            ((a1 >= b1) && (a2 <= b2)));
}

bool rect_intersects(struct drm_clip_rect *rect, struct svdrm_kms_mapping *mapping)
{
    int32_t left_boundary, upper_boundary, right_boundary, lower_boundary;
    bool horizontal_check, vertical_check;

    __SVDRM_TRACE__;

    if (!mapping || !rect) return false;

    left_boundary = mapping->connector_group.crtc.x;
    upper_boundary = mapping->connector_group.crtc.y;
    right_boundary = mapping->connector_group.crtc.x + mapping->connector_group.crtc.mode.hdisplay;
    lower_boundary = mapping->connector_group.crtc.y + mapping->connector_group.crtc.mode.vdisplay;

    horizontal_check = line_intersects(rect->x1, rect->x2, left_boundary, right_boundary);
    vertical_check = line_intersects(rect->y1, rect->y2, upper_boundary, lower_boundary);

    svdrm_debug("Dirty x1: %d, Dirty x2: %d, Dirty y1: %d, Dirty y2: %d\n", rect->x1, rect->x2, rect->y1, rect->y2);
    svdrm_debug("Monitor[%d] x1: %d, Monitor x2: %d, Monitor y1: %d, Monitor y2: %d\n", mapping->key, left_boundary, right_boundary, upper_boundary, lower_boundary);
    svdrm_debug("Horizontal check: %s Vertical check: %s\n", horizontal_check ? "true":"false", vertical_check ? "true":"false");

    return horizontal_check && vertical_check;
}

void rect_clamp_and_translate(struct drm_clip_rect *in, struct drm_clip_rect *out, struct svdrm_kms_mapping *mapping)
{
    int32_t mon_x1, mon_y1, mon_x2, mon_y2;

    __SVDRM_TRACE__;

    if (!in || !out || !mapping) {
        return;
    }

    mon_x1 = mapping->connector_group.crtc.x;
    mon_y1 = mapping->connector_group.crtc.y;
    mon_x2 = mapping->connector_group.crtc.x + mapping->connector_group.crtc.mode.hdisplay;
    mon_y2 = mapping->connector_group.crtc.y + mapping->connector_group.crtc.mode.vdisplay;

    if(in->x1 < mon_x1) {
        out->x1 = mon_x1;
    } else {
        out->x1 = in->x1;
    }

    if(in->x2 >= mon_x2) {
        out->x2 = mon_x2;
    } else {
        out->x2 = in->x2;
    }

    if(in->y1 < mon_y1) {
        out->y1 = mon_y1;
    } else {
        out->y1 = in->y1;
    }

    if(in->y2 >= mon_y2) {
        out->y2 = mon_y2;
    } else {
        out->y2 = in->y2;
    }

    // Display handler expects the dirty regions to be origin to the monitor
    if(out->x1 != 0) {
        out->x1 -= mon_x1;
    }

    if(out->y1 != 0) {
        out->y1 -= mon_y1;
    }

    if(out->x2 != 0) {
       out->x2 -= mon_x1;
    }

    if(out->y2 != 0) {
       out->y2 -= mon_y1;
    }

    svdrm_debug("Monitor x1: %d, Monitor x2: %d, Monitor y1: %d, Monitor y2: %d\n", mon_x1, mon_x2, mon_y1, mon_y2);
    svdrm_debug("Dirty x1: %d, Dirty x2: %d, Dirty y1: %d, Dirty y2: %d\n", in->x1, in->x2, in->y1, in->y2);
    svdrm_debug("Output x1: %d, Output x2: %d, Output y1: %d, output y2: %d\n", out->x1, out->x2, out->y1, out->y2);
}

void rect_clamp(struct drm_clip_rect *in, struct drm_clip_rect *out, struct svdrm_kms_mapping *mapping)
{
    int32_t mon_x1, mon_y1, mon_x2, mon_y2;

    __SVDRM_TRACE__;

    if (!in || !out || !mapping) {
        return;
    }

    mon_x1 = mapping->connector_group.crtc.x;
    mon_y1 = mapping->connector_group.crtc.y;
    mon_x2 = mapping->connector_group.crtc.x + mapping->connector_group.crtc.mode.hdisplay;
    mon_y2 = mapping->connector_group.crtc.y + mapping->connector_group.crtc.mode.vdisplay;

    if(in->x1 < mon_x1) {
        out->x1 = mon_x1;
    } else {
        out->x1 = in->x1;
    }

    if(in->x2 >= mon_x2) {
        out->x2 = mon_x2;
    } else {
        out->x2 = in->x2;
    }

    if(in->y1 < mon_y1) {
        out->y1 = mon_y1;
    } else {
        out->y1 = in->y1;
    }

    if(in->y2 >= mon_y2) {
        out->y2 = mon_y2;
    } else {
        out->y2 = in->y2;
    }

    svdrm_debug("Monitor x1: %d, Monitor x2: %d, Monitor y1: %d, Monitor y2: %d\n", mon_x1, mon_x2, mon_y1, mon_y2);
    svdrm_debug("Dirty x1: %d, Dirty x2: %d, Dirty y1: %d, Dirty y2: %d\n", in->x1, in->x2, in->y1, in->y2);
    svdrm_debug("Output x1: %d, Output x2: %d, Output y1: %d, output y2: %d\n", out->x1, out->x2, out->y1, out->y2);
}

static int32_t
svdrm_fb_dirty(struct drm_framebuffer *framebuffer, struct drm_file *file_priv,
	unsigned flags, unsigned color, struct drm_clip_rect *clips,
	unsigned num_clips)
{
    struct drm_gem_object *gem = NULL;
    struct svdrm_gem_object *svgem = NULL;
	struct svdrm_device *device = NULL;
	uint32_t dirty_width, dirty_height;
	struct svdrm_framebuffer *svdrm_fb = NULL;
	struct svdrm_kms_mapping *mapping = NULL;
	int32_t rc = 0, i, j, display_width, display_height;
    struct drm_clip_rect dummy_rect;

    __SVDRM_TRACE__;

	// Validate paramters
	if (!file_priv || !framebuffer) {
		return -ENOENT;
    }

	// Extract the sv drm_device and verify that we received a valid device
	device = (struct svdrm_device *)file_priv->driver_priv;
	if (!device) {
		return -ENOENT;
    }

	// Convert the drm_framebuffer to a svdrm_framebuffer and obtain a display
	svdrm_fb = to_svdrm_framebuffer(framebuffer);
    if(!svdrm_fb) {
        return -ENOENT;
    }

    gem = svdrm_fb->obj;
    if(!gem) {
        return -ENOENT;
    }

    svgem = to_svdrm_gem_object(gem);
    if(!svgem || !svgem->image) {
        return -ENOENT;
    }

	// Redraw each of the rectangles
    hash_for_each(device->kms_map, j, mapping, hash_node) {
        if (!mapping
         || !mapping->display
         || !mapping->display->framebuffer
         || !mapping->display->invalidate_region) {
            continue;
        }

        display_width = mapping->connector_group.crtc.mode.hdisplay;
        display_height = mapping->connector_group.crtc.mode.vdisplay;

        // If there are no clips, then create one clip the size of the entire desktop space
        if (!num_clips) {
            num_clips = 1;
            clips = &dummy_rect;
            dummy_rect.x1 = dummy_rect.y1 = 0;
            dummy_rect.x2 = framebuffer->width;
            dummy_rect.y2 = framebuffer->height;
        }

        for (i = 0; i < num_clips; i++) {
            if (rect_intersects(&clips[i], mapping)) {
                struct drm_clip_rect source_clip = {0};
                struct drm_clip_rect target_clip = {0};
                int y_src, y_dst;

                rect_clamp_and_translate(&clips[i], &target_clip, mapping);
                rect_clamp(&clips[i], &source_clip, mapping);

                // Calculate the width of the rectangle, which should be the
                // difference of the two x coordinates
                dirty_width = target_clip.x2 - target_clip.x1;

                // Calculate the height of the rectangle, which should be
                // the difference of the two y coordinates
                dirty_height = target_clip.y2 - target_clip.y1;

                if (!display_width || !display_height) {
                    svdrm_debug("Bad resolution\n");
                    return -EINVAL;
                }

                svdrm_debug("//**************************************************************//\n");
                svdrm_debug("Current mapping - Key: %d Res: %dx%d\n", mapping->key, display_width, display_height);
                svdrm_debug("Target Clip: x1: %d, x2: %d, y1: %d, y2: %d \n", target_clip.x1, target_clip.x2, target_clip.y1, target_clip.y2);
                svdrm_debug("Source Clip: x1: %d, x2: %d, y1: %d, y2: %d \n", source_clip.x1, source_clip.x2, source_clip.y1, source_clip.y2);
                svdrm_debug("Dirty WidthxHeight = %dx%d\n", dirty_width, dirty_height);

                if (!mapping->display || !mapping->display->framebuffer) {
                    return -ENOENT;
                }

                // Copy src fb over to the target
                for (y_src = source_clip.y1, y_dst = target_clip.y1; y_dst < target_clip.y2; y_src++, y_dst++) {
                    void *src = svgem->image + (source_clip.x1 * 4) + (y_src * framebuffer->width * 4);
                    void *dst = mapping->display->framebuffer + (target_clip.x1 * 4) + (y_dst * display_width * 4);
                    if (!src || !dst) {
                        return -ENOMEM;
                    }
                    memcpy(dst, src, dirty_width * 4);
                }

                // Mark this region of the framebuffer as requiring a redraw
                // and request that the host redraw this given region
                mapping->display->invalidate_region(mapping->display,
                                                    target_clip.x1, target_clip.y1,
                                                    dirty_width, dirty_height);
            }
        }
    }

	return rc;
}

const struct drm_framebuffer_funcs
svdrm_fb_funcs = {
	.destroy = svdrm_fb_destroy,
	.dirty = svdrm_fb_dirty,
};

static struct sv_cursor*
__svdrm_create_cursor_fb(void)
{
    struct sv_cursor *cursor = NULL;

    __SVDRM_TRACE__;

    // Setup some memory for our cursor buffer
    cursor = kzalloc(sizeof(*cursor), GFP_KERNEL);
    if (!cursor) {
        return ERR_PTR(-ENOMEM);
    }

    // Allocate memory for the actual cursor image
    cursor->image = kzalloc(CURSOR_SIZE, GFP_KERNEL);
    if (!cursor->image) {
        kfree(cursor);
        return ERR_PTR(-ENOMEM);
    }

    // Actually set the cursor image
    cursor->image = (void *)((uintptr_t)cursor->image & PAGE_MASK) + PAGE_SIZE;

    // Set the width and height of the cursor
    cursor->w = CURSOR_WIDTH;
    cursor->h = CURSOR_HEIGHT;

    return cursor;
}

int32_t
__svdrm_sanitize_mode(struct drm_mode_fb_cmd2 *mode)
{
    __SVDRM_TRACE__;

    if (!mode) {
        return -EINVAL;
    }

	mode->width = (mode->width > (MODE_4K_WIDTH * MAX_DISPLAYS)) ? (MODE_4K_WIDTH * MAX_DISPLAYS) : mode->width;
	mode->height = (mode->height > (MODE_4K_HEIGHT * MAX_DISPLAYS)) ? (MODE_4K_HEIGHT * MAX_DISPLAYS) : mode->height;

    mode->width = (mode->width == 0) ? 1024 : mode->width;
    mode->height = (mode->height == 0) ? 768 : mode->height;

	mode->pitches[0] = mode->width * 4;

    return 0;
}

static int32_t
__svdrm_init_drm_framebuffer(struct drm_device *device, struct drm_framebuffer *fb,
            struct drm_mode_fb_cmd2 *mode, const struct drm_framebuffer_funcs *funcs)
{
    int32_t rc = 0;

    __SVDRM_TRACE__;

    // Validate input
    if (!device || !fb || !funcs) {
        return -EINVAL;
    }

    // Fill out the framebuffer metadata from the data passed from userspace.
    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7, 5)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 11, 0))
        drm_helper_mode_fill_fb_struct(device, fb, mode);
    #else
        drm_helper_mode_fill_fb_struct(fb, mode);
    #endif

    // Initialize the drm framebuffer and if we can not, then error out
    rc = drm_framebuffer_init(device, fb, funcs);

    return rc;
}

static struct pv_display*
__get_display_from_mode(struct svdrm_device *device, uint32_t width, uint32_t height)
{
    struct svdrm_kms_mapping *mapping = NULL;
    int32_t i;

    __SVDRM_TRACE__;

    if (!device) {
        return NULL;
    }

    hash_for_each(device->kms_map, i, mapping, hash_node) {
        if(!mapping || !mapping->display || mapping->svfb) continue;
        if (mapping->display_hint.width == width && mapping->display_hint.height == height) {
            svdrm_debug("Found a matching display\n");
            return mapping->display;
        }
    }
    return NULL;
}


static struct drm_framebuffer*
svdrm_fb_create(struct drm_device *dev, struct drm_file *file_priv,
                const struct drm_mode_fb_cmd2 *mode_cmd)
{
    struct drm_gem_object *gem = NULL;
    struct svdrm_gem_object *svgem = NULL;
    struct svdrm_device *svdev = NULL;
    struct svdrm_framebuffer *fb = NULL;
    struct drm_mode_fb_cmd2 mode_command;
    int32_t rc = 0;

    __SVDRM_TRACE__;

    // Validate paramters
    if (!dev || !file_priv || !mode_cmd) {
        return ERR_PTR(-EINVAL);
    }

    // Get the GEM object that's associated with the handle
    gem = svdrm_gem_object_lookup(dev, file_priv, mode_cmd->handles[0]);
    if (!gem) {
        return ERR_PTR(-EINVAL);
    }

    // Get the SV gem object from the regular gem object
    svgem = to_svdrm_gem_object(gem);
    if (!svgem) {
        return ERR_PTR(-ENOENT);
    }

    // Allocate space for our internal svdrm_framebuffer
    fb = kzalloc(sizeof(*fb), GFP_KERNEL);
    if (!fb) {
        return ERR_PTR(-ENOMEM);
    }

    // Obtain the svdrm device from the drm device
    svdev = dev->dev_private;
    if (!svdev) {
        goto err;
    }

    // Adjust the width and height of the buffer if needed, to support our max
    mode_command = *mode_cmd;
    if (__svdrm_sanitize_mode(&mode_command)) {
        goto err;
    }

    svdrm_debug("Creating buffers with pitch %d\n", mode_command.pitches[0]);
    svdrm_debug("Attempting to create a fb of size %dx%d [%d]\n", mode_command.width, mode_command.height, mode_command.fb_id);

    if (mode_command.width == CURSOR_WIDTH && mode_command.height == CURSOR_HEIGHT) {
        svdrm_debug("Creating a cursor fb\n");
        // The framebuffer to create is a cursor fb, initialize the cursor
        fb->cursor = __svdrm_create_cursor_fb();
        if (!fb->cursor) {
            goto err;
        }
        fb->display = NULL;
    } else {
        svdrm_debug("Creating a display fb\n");
        fb->display = __get_display_from_mode(svdev, mode_command.width, mode_command.height);

        /* The drm subsytem first sends out a test buffer of size 1x1. If this is the case, dont print error */
        if (!fb->display && (mode_command.width != 1 && mode_command.height != 1)) {
            svdrm_error("Unable to detect a suitable display for mode: %dx%d\n", mode_command.width, mode_command.height);
        }
        fb->cursor = NULL;

        // Get the SV gem object from the regular gem object
        svgem = to_svdrm_gem_object(gem);
        if (!svgem) {
            __SVDRM_TRACE__;
            return ERR_PTR(-ENOENT);
        }
    }

    // Update additional bookeeping for the fb
    fb->device = svdev;
    fb->obj = gem;
    file_priv->driver_priv = svdev;

    // Fill out the framebuffer metadata from the data passed from userspace.
    //  and initialize the drm framebuffer and if we can not, then error out
    rc = __svdrm_init_drm_framebuffer(svdev->dev, &fb->base, &mode_command, &svdrm_fb_funcs);
    if (rc) {
        goto err;
    }

    return &fb->base;
err:
    if (fb) kfree(fb);
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7, 6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 20, 0))
    if (gem) drm_gem_object_unreference_unlocked(gem);
#else
    if (gem) drm_gem_object_put_unlocked(gem);
#endif
    return ERR_PTR(-EINVAL);
}

#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7, 4)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 7, 0))
struct drm_framebuffer*
__svdrm_fb_create(struct drm_device *dev, struct drm_file *file_priv,
                  const struct drm_mode_fb_cmd2 *mode_cmd)
{
    return svdrm_fb_create(dev, file_priv, mode_cmd);
}
#else
struct drm_framebuffer*
__svdrm_fb_create(struct drm_device *dev, struct drm_file *file_priv,
                  struct drm_mode_fb_cmd2 *mode_cmd)
{
    return svdrm_fb_create(dev, file_priv, (const struct drm_mode_fb_cmd2 *)mode_cmd);
}
#endif

const struct drm_mode_config_funcs
svdrm_mode_funcs = {
	.fb_create = __svdrm_fb_create,
	.atomic_commit = drm_atomic_helper_commit,
	.atomic_check = drm_atomic_helper_check,
};
