export KDIR ?= /lib/modules/$(shell uname -r)/build
export RPM_OUTDIR ?= $(shell pwd)

IVC_INCLUDE_DIR ?= $(KDIR)/include
DDH_INCLUDE_DIR ?= $(KDIR)/include

ccflags-y := -I/usr/include -ldrm -Iinclude/drm -I$(IVC_INCLUDE_DIR) -I$(DDH_INCLUDE_DIR) -g

# Build the svdrm module
svdrm-y := svdrm_drv.o svdrm_fb.o svdrm_gem.o svdrm_kms.o svdrm_pv.o
obj-m += svdrm.o

all: modules

modules:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

modules_install:
	$(MAKE) -C $(KDIR) M=$(PWD) modules_install

deb:
	dpkg-buildpackage -b -uc

deb-dkms:
	dpkg-buildpackage -A -uc

rpm:
	fpm -s dir -t rpm -n svdrm -v 1.0 -p "$(RPM_OUTDIR)" -a all --prefix /usr/src/svdrm-1.0/ -d "dkms" -d "ddh >= 1.0" -d "ivc >= 1.0" -d "libwayland-client" -d "libwayland-server" -d "libwayland-cursor" --after-install rpmdkms/dkmspostinstall --before-remove rpmdkms/dkmspreuninstall --after-remove rpmdkms/dkmspostuninstall --vendor AIS -m AIS@ainfosec.com dkms.conf Makefile svdrm_drv.c svdrm_drv.h svdrm_fb.c svdrm_gem.c svdrm_kms.c svdrm_pv.h svdrm_pv.c svdrm.conf 60-svdrm.conf blacklist-bochs_drm.conf 10-svdrm.rules display_hotplug.sh

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
	rm -f *.rpm
