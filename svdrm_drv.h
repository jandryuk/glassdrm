//
// SVdrm: SecureView's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#ifndef SVDRM_IOCTL_H
#define SVDRM_IOCTL_H

#include <drm/drm.h>
#include <drm/drmP.h>
#include <drm/drm_atomic.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_gem.h>
#include <drm/drm_crtc.h>
#include <drm/drm_crtc_helper.h>
#include <drm/drm_fb_helper.h>
#include <drm/drm_plane_helper.h>
#include <pv_display_helper.h>
#include <linux/version.h>
#include <linux/hashtable.h>
#include <linux/ioctl.h>

/* Define all of the driver information */
#define DRIVER_NAME      "svdrm"
#define DRIVER_MAJOR     3
#define DRIVER_MINOR     0
#define DRIVER_MICRO     6
#define DRIVER_DESC      "Custom DRM driver for SecureView"
#define DRIVER_AUTHOR    "Maurice Gale"
#define DRIVER_DATE      "20160310"
#define DRIVER_LICENSE   "GPL"
#define SVDRM_HASH_BUCKET_ORDER 4

/* Mode Configuration Information */
#define MODE_MIN_HEIGHT 0
#define MODE_MIN_WIDTH 0
#define MODE_4K_WIDTH 3840
#define MODE_4K_HEIGHT 2160
#define MODE_HD_WIDTH 1920
#define MODE_HD_HEIGHT 1200
#define MODE_DEFAULT_BPP 32
#define MODE_DEFAULT_DEPTH 24
#define MODE_DEFAULT_SHADOW 0
#define MODE_DEFAULT_WIDTH 1024
#define MODE_DEFAULT_HEIGHT 768
#define CURSOR_WIDTH 64
#define CURSOR_HEIGHT 64
#define CURSOR_SIZE CURSOR_WIDTH * CURSOR_HEIGHT * 4 + 4096 * 2

/* FBDEV Emnulation Information */
#define FB_MAX_CRTC_SUPPORT 1
#define FB_MAX_CONNECTOR_COUNT 1

/* SV Specific */
#define DISPLAY_HANDLER_PORT 1000
#define DOMAIN_0 0
#define DELAY 1000
#define MAX_DISPLAYS 6
#define RECONNECT_DELAY 500
#define MAX_NAME_SIZE 32

/* Dynamic Debugging bits */
#define svdrm_debug(...) do {pr_debug("svdrm[debug]: " __VA_ARGS__); } while(0);
#define __SVDRM_TRACE__  do {pr_debug("TRACE: %s:%i\n", __func__, __LINE__); } while(0);

/* Always print error and info messages */
#define svdrm_error(...) printk(KERN_ERR "svdrm[error]: " __VA_ARGS__)
#define svdrm_info(...) printk(KERN_ERR "svdrm[info]: " __VA_ARGS__)

// IOCTL Bits
#define SVDRM_IOCTL_OFFSET 0x30
#define SVDRM_IOCTL_GET_STATUS        _IOR('S', 1, uint32_t)
#define SVDRM_IOCTL_GET_DISPLAY_INFO  _IOR('S', 2, uint32_t)
#define SVDRM_IOCTL_UPDATE_GUEST_LAYOUT  _IOR('S', 3, uint32_t)
#define SVDRM_IOCTL_GET_HOST_LAYOUT_CHANGED  _IOR('S', 4, uint32_t)

#ifndef RHEL_RELEASE_CODE
#define RHEL_RELEASE_CODE 0
#define RHEL_RELEASE_VERSION(a,b) (((a) << 8) + (b))
#endif

enum svdrm_driver_status {
    SVDRM_STATUS_UNINITIALIZED,
    SVDRM_STATUS_INITIALIZED,
    SVDRM_STATUS_BUSY,
    SVDRM_STATUS_READY,
    SVDRM_STATUS_UNKNOWN,
};

enum svdrm_types {
    SVDRM_PCI,
    SVDRM_UNKNOWN,
};

enum svdrm_gem_type {
    SVDRM_ERROR,
    SVDRM_SCANOUT,
    SVDRM_CURSOR,
    __SVDRM_GEM_INVALID,
};

enum svdrm_buffer_type {
    SVDRM_BUFFER_1080,
    SVDRM_BUFFER_4K,
    __SVDRM_BUFFER_INVALID,
};

/**
* Extend to functionality of the drm_gem_object by creating a new structure
* that will be driver specific and contains additional fields,
* and then embed the drm_gem_object inside of
* the structure (base).
*/
struct svdrm_gem_object
{
    struct drm_gem_object base;
    enum svdrm_gem_type type;
    struct svdrm_device *svdrm;
    uint8_t *image;
};

// Helper Macro to convert a regular GEM object to a custom svdrm_gem_object.
#define to_svdrm_gem_object(x) container_of(x, struct svdrm_gem_object, base)

struct sv_cursor {
	uint32_t x, y, w, h;
	uint8_t *image;
};

struct svdrm_framebuffer {
    struct drm_framebuffer base;
    struct svdrm_device *device;
    struct drm_gem_object *obj;
    struct pv_display *display;
    struct sv_cursor *cursor;
    uint32_t fb_id;
    spinlock_t lock;
};

// Helper to convert a stander drm_framebuffer object to an svdrm specific one
#define to_svdrm_framebuffer(x) container_of(x, struct svdrm_framebuffer, base)

struct svdrm_work {
    struct work_struct reconnect_task;
    struct svdrm_device *device;
};

struct svdrm_display_hints
{
    // Height and width of the display
    uint32_t width;
    uint32_t height;

    // X and Y position of the display
    uint32_t x;
    uint32_t y;

    // Display Handler key
    uint32_t key;

    // Recommended dumb buffer type (1920x1080 or 4k)
    enum svdrm_buffer_type buffer_type;
};

struct svdrm_kms_connector_group {
    // KMS structs
    struct drm_crtc crtc;
    struct drm_encoder encoder;
    struct drm_connector connector;

    // Crtc/encoder bitshift
    uint32_t index;

    // Planes
    struct drm_plane primary;
    struct drm_plane cursor;
    struct drm_plane overlay;
    uint32_t plane_count;
};

struct svdrm_kms_mapping {
    struct svdrm_kms_connector_group connector_group;

    // SVDRM specifics
    struct svdrm_device *dev;
    struct svdrm_framebuffer *svfb;
    uint32_t key;
    uint32_t display_handle;

    struct pv_display *display;
    struct svdrm_display_hints display_hint;
    bool connected;
    bool connector_registered;
    bool pending_removal;
    enum svdrm_buffer_type buffer_type;
    bool buffer_needs_resize;

    struct hlist_node hash_node;
};

struct svdrm_device
{
    struct mutex lock;
    struct drm_device *dev;
    char name[16];

    struct workqueue_struct *reconnect_queue;

    // Display handler specific stuff
    struct pv_display_provider *provider;
    int32_t configured_displays;
    int32_t advertised_display_count;

    // Display specific structs
    DECLARE_HASHTABLE(kms_map, SVDRM_HASH_BUCKET_ORDER);
    void *cached_cursor;
    int32_t plane_count;
    int32_t cached_mapping;
    bool needs_reconnect;
    bool displays_needs_update;
    struct dh_remove_display *cached_request;
    int32_t dumb_buffer_width;
    int32_t dumb_buffer_height;
    enum svdrm_driver_status driver_status;
    bool host_layout_changed;
};

typedef struct {
    uint32_t num_displays;
    struct svdrm_display_hints display_hint[MAX_DISPLAYS];
    char connector_names[MAX_DISPLAYS][MAX_NAME_SIZE];
    bool is_rhel;
}query_display_info_t;

static
inline void __svdrm_kms_hash_add(struct svdrm_device *dev, struct svdrm_kms_mapping *mapping, uint32_t key)
{
    svdrm_debug(" ---> Adding %d to hash map\n", key);
    hash_add(dev->kms_map, &mapping->hash_node, key);
}

static
inline void __svdrm_kms_hash_del(struct svdrm_kms_mapping *mapping)
{
    if(mapping) {
      svdrm_debug(" <--- Removing %d from hash map\n", mapping->key);
      hash_del(&mapping->hash_node);
    }
}

static
inline void __svdrm_kms_hash_update_key(struct svdrm_device *dev, struct svdrm_kms_mapping *mapping, uint32_t key)
{
    svdrm_debug(" <--> Update key to 0x%08x hash map\n", key);
    hash_del(&mapping->hash_node);
    hash_add(dev->kms_map, &mapping->hash_node, key);
}

/* Helper Macro to convert a regular framebuffer object to a custom svdrm_fb */
#define to_svdrm_fb(x) container_of(x, struct svdrm_fb, base)

/* Functions */

int32_t svdrm_dumb_map_offset(struct drm_file *file_priv, struct drm_device *dev, uint32_t handle, uint64_t *offset);
int32_t svdrm_gem_mmap(struct file *filp, struct vm_area_struct *vma);
struct drm_gem_object* svdrm_gem_object_lookup(struct drm_device *dev, struct drm_file *file, uint32_t handle);
int32_t svdrm_dumb_create(struct drm_file *file, struct drm_device *dev, struct drm_mode_create_dumb *args);
int svdrm_dumb_destroy(struct drm_file *file_priv, struct drm_device *dev, uint32_t handle);

void svdrm_gem_free_object(struct drm_gem_object *object);
void svdrm_kms_cleanup(struct svdrm_kms_connector_group *connector_group);
int32_t svdrm_kms_init(struct svdrm_device *device);
void svdrm_tear_down_display(struct svdrm_device *device, struct pv_display *display);
int32_t svdrm_setup_kms_components(struct svdrm_device *device, struct drm_crtc *crtc, struct drm_encoder *encoder, struct drm_connector *connector);
int32_t svdrm_init_connector(struct drm_device *device, struct svdrm_kms_connector_group *connector_group);
int32_t svdrm_kms_init_connector_group(struct drm_device *device, struct svdrm_kms_connector_group *connector_group);
void svdrm_kms_mode_config_init(struct drm_device *dev);
int32_t svdrm_reinitialize_states(struct svdrm_kms_connector_group *connector_group);
int32_t svdrm_init_crtc(struct drm_device *device, struct svdrm_kms_connector_group *connector_group);
int32_t svdrmInitializeFb(struct svdrm_device *device);
void svdrm_update_display_connection_status(struct pv_display *display, bool connected, int32_t status);
void svdrm_fatal_error_handler(struct pv_display_provider *display);
void svdrm_handle_display_error(struct pv_display *display, void* userData);
void svdrm_pv_remove_display(struct pv_display_provider *provider, struct dh_remove_display *request);
extern const struct drm_mode_config_funcs svdrm_mode_funcs;
extern const struct drm_mode_config_helper_funcs svdrm_drm_mode_config_helpers;
extern const struct drm_framebuffer_funcs svdrm_fb_funcs;
extern const struct drm_crtc_funcs svdrm_crtc_funcs;

#endif // SVDRM_IOCTL_H
